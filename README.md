# Сервис уведомлений
### Тестовое задание для кандидатов-разработчиков

### Установка

Склонируйте репозиторий с GitLab:

`git clone https://gitlab.com/smolin8033/fabrique_test.git`

Выполните команду:

`docker-compose up -d --build`

Документация к API доступна по ссылке: [http://localhost/api/docs/](http://localhost/api/docs/)

При запуске проекта автоматически загружаются фейковые данные для удобства тестирования.