import debug_toolbar
from customer.routers import router as customers_router
from django.contrib import admin
from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from mailing.routers import router as mailings_router
from message.routers import router as messages_router

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/docs/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path("api/", include(customers_router.urls)),
    path("api/", include(mailings_router.urls)),
    path("api/", include(messages_router.urls)),
    path("__debug__/", include(debug_toolbar.urls)),
]
